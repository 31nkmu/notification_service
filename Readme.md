___
# Установка


Для установки потребуются следующие инструменты:
| Инструмент | Описание |
|----------|---------|
| [Python](https://www.python.org/downloads/) |  Язык программирования |
| [Poetry](https://python-poetry.org/) |  Менеджер зависимостей |
| [Redis](https://redis.io/docs/getting-started/installation/install-redis-on-linux/) | Хранилище данных

* Склонируй репозиторий используя команду
```Bash
# клонировать через HTTPS:
$ git clone https://gitlab.com/31nkmu/notification_service.git
# или клонировать через SSH:
$ git clone git@gitlab.com:31nkmu/notification_service.git
$ cd notification_service
```
* Создай виртуальное окружение используя команду
```sh
$ poetry config virtualenvs.in-project true
$ poetry env use <your_python_version>
```

* Активируй виртуальное окружение
```sh
$ source .venv/bin/activate 
```

* Установи зависимости
```sh
$ poetry install
```
* Проведи миграции
```sh
$ make migrate
# создай суперпользователя
$ make createsuperuser
```
## Обычный запуск
* Запусти свой проект используя 3 терминала
###### 1
```sh
$ make run
``` 
###### 2

```sh
$ cd app
$ celery -A config worker -l info
```
###### 3

```sh
$ cd app
$ celery -A config beat
```
---

## Запуск через docker-compose
Тебе понадобятся следующие инструменты
| Инструмент | Описание |
|----------|---------|
| [Docker](https://docs.docker.com/engine/install/ubuntu/) | Докер |
| [docker-compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04) | Докер-композ
Создай .env файл (смотри .env.example)
```sh
touch .env
```
Создай docker/.env файл (смотри docker/.env.example)
```sh
$ touch docker/.env
```

Запусти свой проект через docker-compose
```sh
make compose-collect-up
# создание суперпользователя в контейнере
make compose-createsuperuser
```
Смотри в Makefile для удобной работы с проектом






# API Документация


### Эндпоинты
### Работа с клиентом
- `GET /api/v1/client/`: Получить список всех клиентов.
- `POST /api/v1/client/`: Создать нового клиента.
- `GET /api/v1/client/{id}/`: Получить информацию о клиенте с указанным идентификатором.
- `PUT /api/v1/client/{id}/`: Обновить информацию о клиенте с указанным идентификатором.
- `DELETE /api/v1/client/{id}/`: Удалить клиента с указанным идентификатором.

#### Поля

- `phone_number` (строка, максимум 11 символов): Номер телефона клиента.
- `operator` (строка, максимум 3 символа): Оператор клиента.
- `tag` (строка, максимум 120 символов): Тег клиента.
- `timezone` (строка, максимум 6 символов): Часовой пояс клиента.

### Отправка сообщения (Send Message):

- `POST /api/v1/send_message/`: Создать рассылку.
- `GET /api/v1/client/`: Получить список всех созданных рассылок.
- `GET /api/v1/client/{id}/`: Получить информацию о рассылке с указанным идентификатором.
- `PUT /api/v1/client/{id}/`: Обновить информацию о рассылке с указанным идентификатором.
- `DELETE /api/v1/client/{id}/`: Удалить рассылку с указанным идентификатором.

#### Поля:

- `created_at` (DateTimeField): Дата и время создания объекта сообщения. Это поле автоматически заполняется текущим временем при создании объекта.
- `message` (TextField): Текст сообщения, которое будет отправлено.
- `operator` (CharField): Оператор или код оператора, который будет осуществлять отправку сообщения. Это поле ограничено до 3 символов.
- `tag` (CharField): Тег, связанный с сообщением. Это поле ограничено до 120 символов.
- `stopped_at` (DateTimeField): Дата и время, когда отправка сообщения будет остановлена
- `sent` (BooleanField): Флаг, указывающий на статус отправки сообщения. Если `True`, это означает, что сообщение было успешно отправлено, если `False`, оно еще не было отправлено. По умолчанию, значение равно `False`.


### Примеры использования

Python (используя библиотеку `requests`):

```python
import requests

base_url = "http://localhost:8000/api/v1/client/"
headers = {
      "Content-Type": "application/json"
   }

# получение клиентов
def get_clients():
    url = base_url
    response = requests.get(url, headers=headers)
    return response.json()

# создание клиента
def create_client(phone_number, operator, tag, timezone):
    url = base_url
    data = {
        "phone_number": phone_number,
        "operator": operator,
        "tag": tag,
        "timezone": timezone
    }
    response = requests.post(url, json=data, headers=headers)
    return response.json()

# создание рассылки
def send_message(operator, tag, message, stopped_at=None, created_at=None):
    url = "http://localhost:8000/api/v1/send_message/"
    data = {
        "operator": operator,
        "tag": tag,
        "message": message,
        "stopped_at": stopped_at, 
        "created_at": created_at,
    }
    response = requests.post(url, json=data, headers=headers)
    return response.json()
```

### Дополнительные задания

- подготовил docker-compose для запуска всех сервисов проекта одной командой 
- сделал так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API
- реализовал администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям
- организовал обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в работе внешнего сервиса никак не оказывают влияние на работу сервиса рассылок.
- реализовал дополнительный сервис, который раз в сутки отправляет статистику по обработанным рассылкам на email
