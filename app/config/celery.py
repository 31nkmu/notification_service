import os
import django
from celery import Celery
from django.conf import settings
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')
django.setup()

app = Celery('config')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.beat_schedule = {
    'write_parse': {
        'task': 'applications.message_senders.tasks.send_message',
        'schedule': crontab(day_of_week='*'),
    },
}

app.conf.beat_schedule = {
    'write_parse': {
        'task': 'applications.message_senders.tasks.send_statistic',
        'schedule': crontab(day_of_week='*'),
    },
}
