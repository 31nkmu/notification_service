import requests


class SendMessageAPIInterface:
    url = "https://probe.fbrq.cloud/v1/send"
    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjE4MTI1OTcsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9rYXJiaWxsIn0.21r4dta3f2nqFLbqQfw06EiBpszmPfGwygv3ZH2DrIk"

    def send_msg(self, msg_id, phone, text):
        url = f'{self.url}/{msg_id}'
        headers = {
            "Authorization": f"Bearer {self.token}",
            "Content-Type": "application/json"
        }

        payload = {
            "id": msg_id,
            "phone": phone,
            "text": text
        }

        response = requests.post(url, headers=headers, json=payload)
        return response.status_code
