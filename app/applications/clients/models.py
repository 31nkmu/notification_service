from django.db import models


class Client(models.Model):
    phone_number = models.CharField(max_length=11, unique=True)
    operator = models.CharField(max_length=3)
    tag = models.CharField(max_length=120)
    timezone = models.CharField(max_length=6)

    def __str__(self):
        return self.phone_number
