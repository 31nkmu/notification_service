from rest_framework import viewsets

from applications.clients.models import Client
from applications.clients.serializers import ClientSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
