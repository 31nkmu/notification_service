from django.contrib import admin

from applications.clients.models import Client


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('phone_number', 'operator', 'tag', 'timezone')
