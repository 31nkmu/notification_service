from rest_framework import serializers

from applications.clients.models import Client


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        exclude = ('operator',)

    def validate_phone_number(self, phone_number: str):
        if len(phone_number) != 11:
            raise serializers.ValidationError('Длина номера должна быть 11 цифр')
        if any(sym.isalpha() is True for sym in phone_number):
            raise serializers.ValidationError('Номер должен сосотоять только из цифр')
        if phone_number[0] != '7':
            raise serializers.ValidationError('Номер должен начинаться с 7')
        return phone_number

    def validate_timezone(self, timezone: str):
        timezone = timezone.lower()
        if not timezone.startswith('utc'):
            raise serializers.ValidationError('Часовой пояс должен быть указан в формате "UTC+6"')
        return timezone

    def create(self, validated_data):
        phone_number = validated_data.get('phone_number')
        validated_data['operator'] = phone_number[1:4]
        client_obj = Client.objects.create(**validated_data)
        return client_obj
