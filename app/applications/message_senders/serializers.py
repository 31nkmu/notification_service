import datetime

from rest_framework import serializers

from applications.message_senders.models import MessageSend, Message
from django.utils.timezone import make_aware


class MessageSendSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(required=False)

    class Meta:
        model = MessageSend
        fields = '__all__'

    def validate_operator(self, operator: str) -> str:
        if any(num.isalpha() is True for num in operator):
            raise serializers.ValidationError('Вы можете ввести только цифры')
        return operator

    def validate(self, attrs):
        created_at = attrs.get('created_at')
        stopped_at = attrs.get('stopped_at')
        if not created_at:
            created_at = datetime.datetime.now()
            created_at = make_aware(created_at)
        if created_at and created_at > stopped_at:
            raise serializers.ValidationError('Дата окончания должна быть позже даты создания')
        attrs['created_at'] = created_at
        return attrs

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        queued_count = instance.messages.filter(status='queued').count()
        sent_count = instance.messages.filter(status='sent').count()
        rep['queued_count'] = queued_count
        rep['sent_count'] = sent_count
        return rep


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class MessageDetailSerializer(MessageSendSerializer):

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        queued_messages_objects = instance.messages.filter(status='queued').all()
        sent_messages_objects = instance.messages.filter(status='sent').all()
        queued_messages = MessageSerializer(queued_messages_objects, many=True).data
        sent_messages = MessageSerializer(sent_messages_objects, many=True).data
        rep['queued_messages'] = queued_messages
        rep['sent_messages'] = sent_messages
        return rep
