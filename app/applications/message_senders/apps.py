from django.apps import AppConfig


class MessageSendersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'applications.message_senders'
