from rest_framework import viewsets

from applications.message_senders.models import MessageSend
from applications.message_senders.serializers import MessageSendSerializer, MessageDetailSerializer


class MessageSendViewSet(viewsets.ModelViewSet):
    queryset = MessageSend.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return MessageDetailSerializer
        return MessageSendSerializer
