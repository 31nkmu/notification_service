from django.db import models

from applications.clients.models import Client


class MessageSend(models.Model):
    created_at = models.DateTimeField()
    message = models.TextField()
    operator = models.CharField(max_length=3)
    tag = models.CharField(max_length=120)
    stopped_at = models.DateTimeField()
    sent = models.BooleanField(default=False)

    def __str__(self):
        return self.message


class Message(models.Model):
    STATUS = (
        ('queued', 'queued'),
        ('sent', 'sent')
    )
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=6, choices=STATUS,  default='queued')
    message_send = models.ForeignKey(MessageSend, on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='messages')
