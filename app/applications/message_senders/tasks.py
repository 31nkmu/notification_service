import os
from django.core.mail import EmailMessage

from datetime import datetime

from applications.clients.models import Client
from applications.message_senders.models import MessageSend, Message
from applications.utils.send_msg_api import SendMessageAPIInterface
from config.celery import app
from django.db.models import Q
from django.db import connection
import pandas as pd

from config.settings import EMAIL_HOST_USER

send_msg_api = SendMessageAPIInterface()


@app.task
def send_message():
    current_datetime = datetime.now()
    message_sends = MessageSend.objects.filter(
        Q(created_at__lt=current_datetime) & Q(stopped_at__gt=current_datetime)).filter(sent=False).all()
    for message_send in message_sends:
        clients = Client.objects.filter(operator=message_send.operator).filter(tag=message_send.tag).all()
        for client in clients:
            msg_obj = Message.objects.create(message_send=message_send, client=client)
            status_code = send_msg_api.send_msg(msg_obj.id, client.phone_number, message_send.message)
            if status_code == 200:
                msg_obj.status = 'sent'
                msg_obj.save(update_fields=('status',))
                message_send.sent = True
                message_send.save()


def export_table_to_excel():
    # Получить данные из базы данных
    with connection.cursor() as cursor:
        cursor.execute("""
                SELECT * FROM
                    message_senders_message
                """)
        rows = cursor.fetchall()
        column_names = [desc[0] for desc in cursor.description]

    # Создать DataFrame из данных
    df = pd.DataFrame(rows, columns=column_names)

    df['created_at'] = df['created_at'].apply(lambda dt: dt.replace(tzinfo=None))

    # Сохранить DataFrame в файл Excel
    df.to_excel('статистика.xlsx', index=False)


@app.task
def send_statistic():
    export_table_to_excel()
    # Отправить файл Excel в Telegram
    with open('статистика.xlsx', 'rb') as file:
        file_data = file.read()
        email = EmailMessage(
            'Статистика сообщений',
            'Смотрите статистику во вложенном файле',
            EMAIL_HOST_USER,
            [EMAIL_HOST_USER],
        )
        email.attach('статистика.xlsx', file_data, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        email.send()

    os.remove('статистика.xlsx')
