from django.contrib import admin

from applications.message_senders.models import MessageSend, Message


@admin.register(MessageSend)
class MessageSendAdmin(admin.ModelAdmin):
    list_display = ('message', 'operator', 'tag', 'created_at', 'stopped_at', 'sent')


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'status', 'message_send', 'client')
